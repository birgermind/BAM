# BAM: Birgermind Assistive Metaverse

This project is a basis for an open-source virtual 3D realm, where people are provided with a simplified control interface for discovering and accessing information in an inclusive Web-based Metaverse environment.
